const express = require("express");
const router = express.Router();

// Previously, we entered our function in user routes.
// In fact, routes should just contain the endpoints and it should only trigger the function but it should be where we define the logic of our function.

// The business logic of our API should be in controllers.
// import our user controllers:
const userControllers = require("../controllers/userControllers");

// check the imported userControllers:
// console.log(userControllers);

// import auth to be able to have access and use the verify methods to act as middleware for our routes.
// Middlewares added in the route such as verify() will have access to the req,res objects.
const auth = require("../auth");

// desctructure auth to get only our methods and save it in variables:
const {verify} = auth;

/*
	Updated Route Syntax:

	router.method("/endpoint",handlerFunction)
*/

// Register
router.post("/",userControllers.registerUser);

// Activity S35
// http://localhost:4000/users/details
// verify() is used as a middleware which means our request will get through verify first before our controller

// verify() will not only check the validity of the token but also add the decoded data of the token in the request object as req.user

router.get("/details",verify,userControllers.getUserDetails);

// Route for User Authentication
router.post("/login",userControllers.loginUser);

router.post("/checkEmail",userControllers.checkEmail);

// User Enrollment
// courseId will come from the req.body
// userId will come from the req.user
router.post('/enroll',verify,userControllers.enroll);

module.exports = router;