// Before making a program. Create a model first. "modelSketch.txt"
// After making a model sketch, You will now make the model and connect it to a Database. In this case we will use MongoDB. (ODM)
// Mongoose - ODM library that manages data relationships, validates schemas and simplifies MongoDB Document manipulation via the use of models.
// Schemas - respresentation of a document's structure. Contains a document's expected properties and data types.
// Models - programming interface for querying or manipulating a database. A Mongoose model contains methods that simply such operations. Simply put a connection with the "Collections".

const express = require("express");
// Mongoose is an ODM library to let our ExpressJS API manipulate a MongoDB database.
const mongoose = require("mongoose");

const app = express();
const port = 4000;

/*
	Mongoose Connection

	mongoose.connect() is a method to connect our api with our mongodb database via the use of mongoose. It has 2 arguments. First, is the connection string to connect our API to our mongodB. Second, is an object used to add information between mongoose and mongodb.

	replace/change <password> in the connection string to your db user password

	Reminder: just before the ? in the connection stirng, add the database name.
*/

mongoose.connect("mongodb+srv://admin:admin123@cluster0.cyhnqy7.mongodb.net/bookingAPI?retryWrites=true&w=majority",{

	useNewUrlParser: true,
	useUnifiedTopology: true

});

// We will create notifications if the connection to the db is successful or failed.
let db = mongoose.connection;

// This is to show notifiction of an internal server error from MongoDB. This message will show in both your browser and in your terminal.
db.on('error',console.error.bind(console, "MongoDB Connection Error."));

// If the connection is open and successful, we will out a message in the terminal/gitbash:
db.once('open',() => console.log("Connected to MongoDB."));

// express.json() - to be able to handle the request body and parse it into JS Object
app.use(express.json());

// import our routes and use it as middleware.
// Which means, that we will be able to group together our routes
const courseRoutes = require('./routes/courseRoutes');

// console.log(courseRoutes);
// use our routes and group them together under '/course'
// our endpoints are now prefaced with /courses
app.use('/courses',courseRoutes);

// import the user routes
const userRoutes = require('./routes/userRoutes');
app.use('/users',userRoutes);

app.listen(port,() => console.log(`Server is running at port ${port}`));
// Close port - npx kill-port 4000