Mini Activity:

You can follow our step by step in the previous repo.

Create 2 new routes with the same endpoint/courses

-get method route to show a message:
	"This route will get all course documents"

-post method route to show a message:
	"This route will create a new course document"

Run your server with gitbash/terminal and send a screenshot of at least one of the responses from postman+


-----------------------------------------------------------
-----------------------------------------------------------
Creating an ExpressJS API for the first time:

In your project folder, create a file called index.js
Then, open your project folder on gitbash/terminal.
Then, create your package.json which contains the details of your new project with the following command on gitbash/terminal:

	npm init


Just press enter to get through all questions.
Once package.json is created, you can now install the express package:

	npm install express

NOTE: you can install them simultaneously.

	ex: npm install express nodemon


You can go to your package.json under the dependencies if express was installed properly.

You can now create your own expressjs api.

In your index.js:
First, import express and save it in a variable called express.

	const express = require("express");


Second, run and create a new api with express() and save it in a variable called app.

	const app = express();


Third, create variable called port and assign the port number our api/server will run on.

	const port = 4000


Fourth, assign the server using .listen() and run a console log to indicate that the server is running:

	app.listen(port,()=>console.log(`Server is running at port ${port}`));



Congratulations, you now have an ExpressJS API.

Fifth, install nodemon

	npm install nodemon

Sixth, add these in the package.json under scripts:

	"dev": "nodemon index",
	"start": "node index"

Seventh, add gitignore for files that we do not need to push or will cause problems when pushing.

	.gitignore

		type the file names of folder/files that you will not push.

Eigth, Install mongoose
	
	npm install mongoose

Ninth, Install Bcrypt and jsonwebtoken

	npm install bcrypt jsonwebtoken